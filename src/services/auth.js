const favoriteBook = {
    title: "The Guards",
    author: "Ken Bruen"
}

// a Book class using ES6 class syntax
class Book {
    constructor(title, author) {
        this.title = title;
        this.author = author;
    }

    describeBook() {
        let description = "SDFSDF";//this.title + " by " + this.author + ".";
        return description;
    }
}

module.exports = Book;