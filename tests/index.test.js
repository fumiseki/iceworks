const add = (a, b) => a + b;

test('2 + 3 = 5', () => {
    expect(add(2, 3)).toBe(5);
});